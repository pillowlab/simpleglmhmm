function [mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT)
% [mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT)
%
% Generate some fake data from a simulated HMM with Bernoulli outputs and
% pass back data + plus true params +latents 
%
% Inputs:
% ------
%  nStates - number of latent states
%     nObs - number of observation classes (2 = Bernoulli) 
%     nXdims - number of observed states
%       nT - number of time bins
%
% Output:
% ------
%    mm - model struct with fields:
%           .A [k x k] - transition matrix (k states)
%           .Wts [xdim x nObs x k] - GLM filters for each state
%   xstim [nx x T] - data (indicating class observed in each time step)
% zlatent [1 x T] - latent state at each time bin
%    yobs [1 x T] - observation at each time bin
%   y1hot [2 x T] - one-hot representation of outputs

% Sample transition matrix from Dirichlet distr
alpha_diag = 10;  % concentration param added to diagonal (higher makes more uniform)
alpha_full = 1;  % concentration param for other entries (higher makes more uniform)
A = gamrnd(alpha_full*ones(nStates) + alpha_diag*eye(nStates),1);
A = A./repmat(sum(A,2),1,nStates); % normalize so rows sum to 1

% Make GLM filters
Wts = randn(nX,(nObs-1),nStates); % random filters

% Set into struct
mm.A = A;
mm.Wts = Wts;

%% Generate simulated dataset

xstim = randn(nX,nT); % make stimuli
zlatent = zeros(1,nT); % latents
yclass = zeros(1,nT); % observations

% Compute GLM probabilities
try
    unnormPy = exp(pagemtimes(xstim',Wts)); % only works in r2020B 
catch
    unnormPy = permute(exp(xstim'*reshape(Wts,nX,[])),[1,3,2]); % older syntax
end

% Sample first state and observation
zlatent(1) = randsample(nStates,1); % sample first state from uniform
yclass(1) = randsample(nObs,1,true,[1,unnormPy(1,:,zlatent(1))])-1; % sample first obs

for jj = 2:nT
    zlatent(jj) = randsample(nStates,1,true,A(zlatent(jj-1),:)); % sample state
    yclass(jj) = randsample(nObs,1,true,[1,unnormPy(jj,:,zlatent(jj))])-1; % sample observation    
end

% Convert classes to one-hot representation
if nargout>3
    y1hot = full(convertToOneHot(yclass+1,nObs));
end

% If using Bernoulli GLM, convert weights to matrix
if (nObs==2)
    mm.Wts = squeeze(mm.Wts);
end