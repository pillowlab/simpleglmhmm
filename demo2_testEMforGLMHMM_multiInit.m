% Test EM algorithm for fitting a GLM-HMM to simulated data using multiple
% initializations

%  JP (Jun 2019).

% add directories
addpath tools; 
addpath inference; 

%% 1. Generate simulated dataset

% Set parameters: transition matrix and emission matrix
nStates = 3; % number of states
nObs = 2; % number of observation classes (2 = Bernoulli)
nX = 5;  % number of input dimensions
nT = 2e3; % number of time bins

% Generate data from a simulated HMM
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);
stateFractions = mean(full(convertToOneHot(zlatent)'));
fprintf('State occupancy percentages:\n [ ');
fprintf('%.1f  ', 100*stateFractions);
fprintf('] %% \n');

%% 2. Fit standard Bernoulli GLM (ie 1-state model)
fprintf('\n------------\nFitting GLM...\n');

lfunc = @(w)(neglogli_bernoulliGLM(w,xstim',yclass')); % neglogli function handle
opts1 = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','display','off');
wts0 = randn(nX,1)*.1;
[Wglm,nlogli,H] = fminunc(lfunc,wts0,opts1);

tw = 1:nX;
clf;subplot(221); 
plot(tw,mm.Wts,'linewidth',2);
%h = get(gca,'colororder'); set(gca,'colororder',h(1:2,:));
lw = 2;
hold on; plot(tw,Wglm,'k--','linewidth',lw); hold off;
xlabel('coeffient'); title('true weights');
wlabels = {'W_1','W_2','W_3','W_4','W_5'}; % state labels
legend(wlabels{1:nStates}, 'W_{glm}');
set(gca,'xtick',1:nX); drawnow;


%% 3. Initialize from true params and run EM
fprintf('\n------------\nRunning EM initialized from true params...\n\n');
 
% Run forward-backward with true params to estimate of latent
[logpTrue,gamsTrue] = runFB_GLMHMM_stable(mm.A,mm.Wts,xstim,y1hot);

% Initialize EM from true params to see upper bound (presumably) on logli
optsEM.maxiter = 200;  % max # of EM iterations
optsEM.dlogptol = 1e-4; % stop when change in log-likelihood falls below this
optsEM.display = 5;
[Abest,Wbest,logpBest,logpTraceBest,jStop] = runEMforGLMHMM(mm.A,mm.Wts,xstim,y1hot,optsEM);

subplot(223); cla reset; hold on;
plot([1 optsEM.maxiter],logpTrue*[1 1], 'k', ...
    1:jStop,logpTraceBest(1:jStop),'k--', 'linewidth', lw);
drawnow;

%% 4. Fit GLM-HMM by running EM from many random initializations

optsEM.display = inf;
nrpts = 20; % number of times to run EM
pTrace = zeros(optsEM.maxiter,nrpts); % stored log-likelihood traces
logp_store = zeros(nrpts,1);
What_store = zeros(nX,nStates,nrpts);
Ahat_store = zeros(nStates,nStates,nrpts);

fprintf('\n------------------\nRunning EM with random initializations:\n');
fprintf('Repeat #: \n');
for jj = 1:nrpts

    % Initialize A
    A0 = eye(nStates)+.1*rand(nStates);
    A0 = A0 ./ sum(A0,2); % normalize so rows sum to 1

    % Initialize Weights
    W0 = Wglm + randn(nX,nStates)*norm(Wglm)*.25;
    
    % --- run EM -------
    [Ahat,What,logp,logpTrace,jStop] = runEMforGLMHMM(A0,W0,xstim,y1hot,optsEM);
    
    % --- store outputs of this run -------
    pTrace(:,jj) = logpTrace'; % store log likelihood trace
    What_store(:,:,jj) = What;
    Ahat_store(:,:,jj) = Ahat;
    logp_store(jj) = logp;
    
    % ---- make plots -------
    plot([1 optsEM.maxiter],logpTrue*[1 1], 'k', 1:jStop,logpTrace(1:jStop),'-', 'linewidth', lw); 
    xlabel('EM iteration');
    ylabel('log p(Y|theta)');
    drawnow;
    
    fprintf(' %3d ', jj);
    if mod(jj,10)==0
        fprintf('\n');
    end

end
fprintf('\n\n');
hold off;

[logpML,imax] = max(logp_store);
WhatML = What_store(:,:,imax);
AhatML = Ahat_store(:,:,imax);

%% 5. Make plots

% Permute states to find best match to true model states
Mperm = computeAlignmentPermutation(mm.Wts,WhatML);
AhatML = Mperm*AhatML*Mperm';
WhatML = WhatML*Mperm';

subplot(222);
hist(logp_store-logpTrue); 
title('relative log-likelihoods');
xlabel('logp - logpTrue');
ylabel('count');

subplot(224); cla;
h1=plot(tw,mm.Wts,'linewidth',2);
h = get(gca,'colororder'); set(gca,'colororder',h(1:nStates,:)); hold on;
h2=plot(tw,Wbest,':o', 'linewidth',1); 
h3=plot(tw,WhatML,'--', 'linewidth',2); 
hold off;
xlabel('coeffient'); title('weights');
set(gca,'xtick',1:nX);
legend([h1(1),h2(1),h3(1)],'true','initTrue','ML');

fprintf('Relative log-likelihoods (to True)\n');
fprintf('----------------------------------\n');
fprintf('InitFromTrue: %.3f\n',logpBest-logpTrue);
fprintf('InitfromRand:  %.3f\n',logpML-logpTrue);
