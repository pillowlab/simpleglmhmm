% Test that multinomial and Bernoulli GLM log-likelihoods match when k=2

% Set paths
addpath ..
addpath ../tools; % add directories
addpath ../inference;

% =========================================================================
% 1. Set GLM-HMM parameters:
nStates = 1; % number of states
nObs = 2;  % number of outputs
nX = 5;  % number of input dimensions
nT = 101; % number of time bins

% Sample dataset from a randomly generated GLM-HMM 
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);

% =========================================================================
% 2. Compute Bernoulli and multnomial GLM log likelihoods 

w0 = randn(nX,1);
l1 = neglogli_bernoulliGLM(w0,xstim',yclass');
l2 = neglogli_multinomGLM(w0,xstim',y1hot');

% =========================================================================
% 3. Report test results

if abs(l1-l2)<1e-8
    fprintf('UNIT TEST (multinom vs Bernoulli logli): PASSED\n');
else
    fprintf('\n\nLog-lis: %f %f\n\n', l1, l2);
    error('Failed Unit Test: multinomial and Bernoulli log-li');
end
fprintf('----------------------------------------------------------------\n');

% =========================================================================
% 4. Compute M-step Bernoulli and multnomial GLM log likelihoods 

pstate = rand(nT,1); % generate random weights between 0 and 1
l1 = neglogli_Mstep_bernoulliGLM(w0,xstim',yclass',pstate);
l2 = neglogli_Mstep_multinomGLM(w0,xstim',y1hot',pstate);

if abs(l1-l2)<1e-8
    fprintf('UNIT TEST (multinom vs Bernoulli logli M-STEP): PASSED\n');
else
    fprintf('\n\nLog-lis: %f %f\n\n', l1, l2);
    error('Failed Unit Test: multinomial and Bernoulli log-li M-STEP');
end
fprintf('----------------------------------------------------------------\n');
