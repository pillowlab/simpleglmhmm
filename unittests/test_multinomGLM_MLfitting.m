% Test ML fitting of multonmial logistic regression (multinomial-GLM) model
%
% last edited: Jan 2021 (JWP)

% Set path
addpath ..
addpath ../tools; 
addpath ../inference;

% Set random numer generator state
rng(0);

% 1. Generate simulated dataset

% Set parameters: transition matrix and emission matrix
nStates = 1; % number of states
nObs = 3;  % number of outputs
nX = 5;  % number of input dimensions
nT = 1e4; % number of time bins

% Generate data from a simulated HMM
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);

% 2. Fit standard multinomial GLM (ie 1-state model)

lfunc = @(w)(neglogli_multinomGLM(w,xstim',y1hot')); % neglogli function handle
opts1 = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','display','off');
wts0 = randn(nX*(nObs-1),1)*.1;
[Wglm_ml,nlogli,H] = fminunc(lfunc,wts0,opts1);

err = mm.Wts(:)-Wglm_ml; % error between true and estimated weights

% --- 3. Report unit test results ----------------
if ~any(abs(err)>0.1)
     fprintf('UNIT TEST (multinomGLM_MLfitting): PASSED\n');
 else
     warning('Failed Unit Test: multinomGLM_MLfitting\n');
end

