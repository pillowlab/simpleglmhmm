% RUN_TESTS

% Runs all unit tests contained in this directory

% Get list of files in current directory
D=dir;

% Run all scripts except this one
for jj=3:length(D)
    nm = D(jj).name;
    if ~strcmp(nm(1:3),'RUN')
        fprintf('\n\nRUNNING UNIT TEST: %s\n', nm);
        eval(nm(1:end-2))
    end
end
