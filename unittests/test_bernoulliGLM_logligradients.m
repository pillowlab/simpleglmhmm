% Test gradients and Hessian of Bernoulli GLM log-likelihood
%
% DEPENDENCIES:
%    HessCheck.m - from: https://bitbucket.org/pillowlab/ncclabcode/

% Set paths
addpath ..
addpath ../tools; % add directories
addpath ../inference;

% =========================================================================
% 1. Set GLM-HMM parameters: 

nStates = 1; % number of states
nObs = 2;  % number of outputs
nX = 5;  % number of input dimensions
nT = 101; % number of time bins

% Generate data from a simulated GLM-HMM
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);

% =========================================================================
% 2. Evaluate Bernoulli GLM log-likelihood 

lfunc = @(w)(neglogli_bernoulliGLM(w,xstim',yclass')); % neglogli function handle
wts0 = randn(nX*(nObs-1)*nStates,1)*.1; % random set of initial weights
[~,gradDiff,hessDiff] = HessCheck(lfunc,wts0); % check gradient and Hessian

% =========================================================================
% 3. Report unit test results (part 1)

if abs(gradDiff)<1e-3 && abs(hessDiff)<1e-3
     fprintf('UNIT TEST (bernoulliGLM_logligradients) -- logli grad & Hessians: PASSED\n');
 else
     warning('Failed Unit Test: bernoulliGLM_logligradients\n');
end
fprintf('----------------------------------------------------------------\n');

% =========================================================================
% 4. Evaluate Weighted Bernoulli GLM log-likelihood (for M step) 

pstate = rand(nT,1); % random weights between 0 and 1
lfunc = @(w)(neglogli_Mstep_bernoulliGLM(w,xstim',yclass',pstate)); % neglogli function handle
[~,gradDiff,hessDiff] = HessCheck(lfunc,wts0); % check gradient and Hessian


% =========================================================================
% 5. Report unit test results (part 2)

if abs(gradDiff)<1e-3 && abs(hessDiff)<1e-3
    fprintf('UNIT TEST (bernoulliGLM_logligradients for M-STEP) -- logli grad & Hessians: PASSED\n');
 else
     warning('Failed Unit Test 2: bernoulliGLM_logligradients for M-STEP\n');
end
fprintf('----------------------------------------------------------------\n');
