function X = convertToOneHot(C,nClass)
% X = convertToOneHot(C,nClass)
% 
% Convert class labels to a 1-hot representation
%
% Input:
% -------
%       C [1 x nT] - class label
%  nClass [1 x 1]- number of total class labels (optional)
%  
%
% Output:
% -------
%  X [nC x nT] - one hot data (each column is a one-hot vector)


if nargin < 2
    nClass = max(C);
end

nT = length(C);
X = sparse(C,1:nT,true,nClass,nT);
