function [P,Jp,H] = convertToDistr(M)
% P = convertToDistr(M)
% 
% Convert to distribution from log odds params
% P = exp(M)/(1+sum(exp(M))
%
% Input:
% -------
%  M [m x np-1] - each row specifies logit params for a dist
%
% Output:
% -------
%  P [m x np] - each row is a distribution


% get size of M
[nrows,np] = size(M); % 

P = [exp(M), ones(nrows,1)]; % exponentiated logit values
P = P ./ sum(P,2); % normalize so each row sums to 1

% Compute Jacobian
if nargout > 1
    Jeye = [repmat(eye(np),1,nrows); zeros(1,np*nrows)];
    pvec = reshape(P(:,1:np)',[],1);
    Jp = Jeye - ones(np+1,1)*pvec';
    
    % Make into block diagonal matrix
    Jp = mat2cell(Jp,np+1,repmat(np,1,nrows));
    Jp = blkdiag(Jp{:});
end

% Compute Hessian
if nargout > 1
    H = zeros(np,np,nrows);
    for j = 1:nrows
        pp = pvec((j-1)*np+1:j*np); 
        H(:,:,j) = diag(pp)-pp*pp';
    end
end
