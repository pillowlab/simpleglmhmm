function C = convertFromOneHot(X)
% C = convertFromOneHot(X)
% 
% Convert from 1-hot representation to class labels
%
% Input:
% -------
%  X [nC x nT] - data (each column is a one-hot vector)
%
% Output:
% -------
%  C [1 x nT] - class label

[~,C] = max(X);
