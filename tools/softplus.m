function [f,df,ddf] = softplus(x)
%  [f,df,ddf] = softplus(x);
%
%  Stably computes the soft-rectification or "softplus" function:
%     f(x) = log(1+exp(x))
%  and its first (df) and second (ddf) derivatives, if desired. 
%
%  For small values (x < -20), it uses the approximation:
%     f(x) = df(x) = ddf(x) = exp(x) 
%  
%  For large values (x > 500), it uses the approximation:
%     f(x) = x, df(x) = 1, ddf(x) = 0
%
%  Inputs:
%  -------
%      x: array of values 
%      
%  Outputs:
%  --------
%      f:   array of values of f(x) that is the same size as x
%      df:  array of values of df(x) that is the same size as x
%      ddf: array of values of ddf(x) that is the same size as x
%      
% 2018 - Adam Charles & Jonathan Pillow


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = log(1+exp(x));                % Calculate funciton values

if nargout > 1
    df = exp(x)./(1+exp(x));      % 1st derivatives
end
if nargout > 2
    ddf = exp(x)./(1+exp(x)).^2;  % 2nd derivatives
end

% Check for small values (to avoid underflow errors)
if any(x(:)<-20)
    iix = (x(:)<-20);
    f(iix) = exp(x(iix));
    df(iix) = f(iix);
    ddf(iix) = f(iix);
end

% Check for large values (to avoid overflow errors)
if any(x(:)>500)
    iix = (x(:)>500);
    f(iix) = x(iix);
    df(iix) = 1;
    ddf(iix) = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
