function M = convertFromDistr(P)
% P = convertToDistr(M)
% 
% Convert from distribution P to from log odds params M:
%    P = exp(M)/(1+sum(exp(M))
%
% Input:
% -------
%  P [m x np] - each row is a distribution
%
% Output:
% -------
%  M [m x np-1] - each row specifies logit params for a dist

minval = 1e-13;  % set a minimum value to avoid zeros

logP = log(max(P,minval));
M = logP(:,1:end-1) - logP(:,end); % subtract prob of last row
