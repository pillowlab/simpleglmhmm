% Test GLM-HMM with multi-class ("multinomial logistic regression") outputs  
%
% last edited: Jan 2021 (JWP)

addpath tools; % add directories
addpath inference;

%% 1. Generate simulated dataset

% Set parameters: transition matrix and emission matrix
nStates = 3; % number of states
nObs = 4;  % number of outputs
nX = 5;  % number of input dimensions
nT = 2e4; % number of time bins

% Generate data from a simulated HMM
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);
stateFracs = mean(full(convertToOneHot(zlatent,nStates)),2)';
fprintf('------------------------\n');
fprintf('Latent state occupancies:\n');
fprintf([repmat('%.2f  ',1,nStates),'\n'],stateFracs);
fprintf('-------------------------\n');

% ==== Plot params and data ====
subplot(221);
imagesc(mm.A); axis image; 
xlabel('state at time t+1'); ylabel('state at time t');
set(gca,'xtick',1:nStates,'ytick',1:nStates);
title('transition matrix A'); colorbar;

subplot(223);
tw = 1:nX; tz = 1:nStates;
colrs = get(gca,'colororder'); lw = 2;
plot(tw, tw*0, 'k--'); hold on;
wplot = reshape(mm.Wts,nX,nObs-1,nStates);
for jj = 1:nStates
    plot(tw, wplot(:,:,jj),'color', colrs(jj,:),'linewidth', lw); 
end
hold off;
xlabel('regressor'); ylabel('weight');
set(gca,'xtick',tw);
title('GLM weights');
labels = {'state 1 wts', 'state 2 wts','state 3 wts','state 4 wts'};
lineobj = get(gca,'children');
legend(lineobj(2:(nObs-1):(nObs-1)*nStates+1), labels{1:nStates});

subplot(222);
tt = 1:min(nT,100); % number of time bins to plot
plot(tt, zlatent(tt), '-o', 'linewidth', lw)
set(gca,'ytick',tz,'ylim',[0 nStates]+0.5);
title('true latent'); ylabel('state');

subplot(224);
plot(tt, yclass(tt), '-o', 'linewidth', lw);
set(gca,'ytick',tw,'ylim',[0 nObs]-0.5);
title('observations'); xlabel('time'); ylabel('observation');
drawnow;


%% 2. Fit standard multinomial GLM (ie 1-state model)

fprintf('\n--------------------------\n');
fprintf('Fitting multinomial GLM...\n');
fprintf('--------------------------\n');

% Optimization options
opts1 = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','display','iter');

% Make function handle
lfunc = @(w)(neglogli_multinomGLM(w,xstim',y1hot')); % neglogli function

wts0 = randn(nX*(nObs-1),1)*.1; % initial weights

% Run ML fitting of 1-state model
tic;
[Wglm_ml,nlogli,H] = fminunc(lfunc,wts0,opts1);
toc;

% Plot results
plot(tw,mm.Wts(:,:,1),'linewidth',lw); hold on; 
plot(tw,reshape(Wglm_ml,nX,[]),'k--','linewidth',lw); 
xlabel('coeffient'); title('1-state model fit'); hold off;
legend('W_1', 'W_2', 'W_{glm}'); drawnow;
set(gca,'xtick',tw);

%% 3. Run EM to estimate model params from a random initialization

% Run forward-backward w/ true params to get optimal latent probabilities 
[logpTrue,gamsTrue] = runFB_GLMHMM_stable(mm.A,mm.Wts,xstim,y1hot);

fprintf('\n------------------------------------------------\n');
fprintf('Running EM initialized from GLM fit (+ noise)...\n');
fprintf('------------------------------------------------\n');

optsEM.maxiter = 100;  % max # of EM iterations
optsEM.dlogptol = 1e-2; % stop when change in log-likelihood falls below this
optsEM.display = 10;

% Initialize A
A0 = 1*eye(nStates)+.2*rand(nStates);
A0 = A0 ./ sum(A0,2); % normalize so rows sum to 1

% Initialize Weights
W0 = reshape(Wglm_ml,nX,nObs-1) + randn(nX,nObs-1,nStates)*.25;

% --- run EM -------
tic;
[Ahat,What,logp,logpTrace,jStop,dlogp,gams] = runEMforGLMHMM_multinom(A0,W0,xstim,y1hot,optsEM);
tEM = toc;

%% 4. Display results

% Permute states to find best match to true model states
Wtrue = reshape(mm.Wts,nX*(nObs-1),nStates); % reshape to matrix
What  = reshape(What,nX*(nObs-1),nStates);   % reshape to matrix
Mperm = computeAlignmentPermutation(Wtrue,What); % compute permutation
Ahat = Mperm*Ahat*Mperm';
What = What*Mperm';
What = reshape(What,nX,nObs-1,nStates); % reshape to tensor


% ---- make plots -------
subplot(223);
plot([1 min(jStop+20,optsEM.maxiter)],logpTrue*[1 1], 'k',...
    1:jStop,logpTrace(1:jStop),'-', 'linewidth', lw);
xlabel('EM iteration');
ylabel('log p(Y|theta)');
title('EM path');

if jStop==optsEM.maxiter
    fprintf('EM terminated after max # iters (%d) reached (dlogp = %.4f)\n',jStop,dlogp);
else
    fprintf('EM terminated after %d iters (dlogp = %.4f)\n',jStop,dlogp);
end
fprintf('\nrelative final log-likelihood: %.2f \n', logp-logpTrue);
if logp>logpTrue, fprintf('(SUCCESS!)\n');
else,   fprintf('(FAILED to find optimum!)\n');
end
fprintf('\n------------------------------------------\n');
fprintf('Total compute time for EM: %.3f seconds\n',tEM);
fprintf('------------------------------------------\n');

subplot(224);
colrs = get(gca,'colororder');
plot(tw, tw*0, 'k--'); hold on;
for jj = 1:nStates
    plot(tw, wplot(:,:,jj),'color', colrs(jj,:),'linewidth',lw); 
    plot(tw, What(:,:,jj), '--', 'color', colrs(jj,:), 'linewidth', lw+1);
end
hold off;
xlabel('regressor'); ylabel('weight');
set(gca,'xtick',tw);
title('GLM weights');
labels = {'state 1 wts', 'state 2 wts','state 1 Estimate','state 2 Estimate'};
lineobj = get(gca,'children'); % get line handles
% Figure out which lines to put into legend (UGLY)
linenums = 2:(2*(nObs-1)):(nObs*min(2,nStates)+1);
linenums = length(lineobj)-[linenums,linenums+(nObs-1)]+1;
legend(lineobj(linenums),labels(1:2*min(2,nStates)));


