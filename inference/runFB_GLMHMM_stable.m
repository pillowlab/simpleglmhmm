function [logp,gams,xisum,logcs] = runFB_GLMHMM_stable(A,W,xdat,ydat)
% [logp,gams,xisum,logcs] = runFB_GLMHMM_stable(A,W,xdat,ydat)
% 
% Run forward-backward algorithm (E-Step) for GLM-HMM with discrete
% observations using logSumExp trick (to avoid underflow)
%
% - uses formulas for hat-alpha and hat-Beta from Bishop (pg 627-629)
% 
% - "stable" denotes use of logSumExp trick (which is slightly slower)
%
%
% Inputs:
% -------
%      A [k x k] - transition matrix (k states)
%      W [d x k] - GLM weights for each state
%   xdat [nx x T] - stimulus for each time bin
%   ydat [ny x T] - response for each time bin (one-hot representation)
%
% Outputs:
% -------
%    logp [1 x 1] - log marginal probability: log p(X|theta)
%    gams [k x T] - marginal posterior over state at each time: p(z_t|X)
%  xissum [k x k] - summed marginal over pairs of states: p(z_t, z_{t+1}|X)
%   logCs [1 x T] - log conditional marginal li: log p(y_t | y_{1:t-1}) 
%
% Note: requires MATLB v2016b or later (for expansion operations)

% Extract sizes
nStates = size(A,1);  
nT = size(xdat,2);
nObs = size(ydat,1);  % assumed to be 2 for now!!

% Set initial state probability
logpi0 = (ones(1,nStates)/nStates); % assume uniform prior over initial state

% ============= ORIGINAL BERNOULLI CODE ================
% % Compute GLM probabilities (nT x nStates x 2)
% logpy = zeros(nT,nStates,nObs);
% xproj = xdat'*W;
% logpy(:,:,2) = xproj;  % set second sheet to hold xw
% logpy = logpy - logsumexp(logpy,3); % normalize so sums to one
% logpy = sum(logpy.*permute(ydat,[2 3 1]),3); % limit to observed likelihood terms only
% =======================================================

% Check shape of W for Bernoulli vs. Multinomial 
if (nObs==2) && (size(W,3)==1) && (size(W,2)==nStates)
    % Bernoulli case: permute indices so each page has per-state weights
    W = permute(W,[1 3 2]);  
elseif (size(W,3)~=nStates) || (size(W,2)~=(nObs-1))
    error('Weights matrix size mismatch to number of states & observations');
end

% Compute GLM likelihood terms
xproj = zeros(nT,nObs,nStates); 
xproj(:,2:nObs,:) = pagemtimes(xdat',W); % project stimuli onto weights
logpy = xproj - logsumexp(xproj); % compute all likelihoods
logpy = sum(logpy.*ydat',2);  % select only likelihoods for observed data
logpy = squeeze(logpy); % convert to matrix (nT x nStates)

%% Forward pass
logaa = zeros(nStates,nT); % forward probabilities p(z_t | y_{1:t})
logbb =  zeros(nStates,nT); % backward: p(y_{t+1:T} | z_t)/p(y_{t+1:T} | y_{1:T})
logcs = zeros(1,nT); % forward marginal likelihoods: p(y_t|y_{1:t-1})

% First bin
logpxz = logpi0 + logpy(1,:); % log joint: log P(y_1,z_1) 
logcs(1) = logsumexp(logpxz,2); % log-normalizer
logaa(:,1) = logpxz' - logcs(1); % log conditional log P(z_1|y_1) 

% Remaining time bins
for jj = 2:nT
    logaaprior = log(exp(logaa(:,jj-1))'*A);  % propagate uncertainty forward
    logpxz = logaaprior + logpy(jj,:); % joint P(y_{1:t},z_t)
    logcs(jj) = logsumexp(logpxz,2);  % conditional P(y_t|y_{1:t-1})
    logaa(:,jj) = logpxz' - logcs(jj); % conditional P(z_t|y_{1:t})
end

%% Backward pass 
for jj = nT-1:-1:1
    logbbpy = logbb(:,jj+1) + logpy(jj+1,:)';
    logbb(:,jj) = log(A*exp(logbbpy)) - logcs(jj+1);
end

%% Compute outputs

% marginal likelihood
logp = sum(logcs);

% marginal posterior over states p(z_t | Data)
loggams = logaa + logbb; 
gams = exp(loggams);

% Compute only sum of xis 
logA = log(A);
if nargout > 2
     xisum = zeros(nStates,nStates); % P(z_n-1,z_n|y_1:T)
     for jj = 1:nT-1
         
         % % Standard version (without logSumExp trick):
         % xisum = xisum + (aa(:,jj) * (bb(:,jj+1)'.*Py(jj+1,:,ydat(:,jj+1))) .*A)/cs(jj+1);

         % Using logSumExp:
         xisum = xisum + exp(logA + (logaa(:,jj)-logcs(jj+1)) + (logbb(:,jj+1)' + logpy(jj+1,:)));
         
     end

end
