function [logp,gams,xisum,cs,aa,bb] = runFB_GLMHMM(A,W,xdat,ydat)
% [logp,gams,xisum,cs,aa,bb] = runFB_GLMHMM(A,W,xdat,ydat)
% 
% Run forward-backward algorithm (E-Step) for GLM-HMM with discrete
% observations.
%
% Uses formulas for hat-alpha and hat-Beta from Bishop (pg 627-629)
%
% Inputs:
% -------
%      A [k x k] - transition matrix (k states)
%      W [d x k] - GLM weights for each state
%   xdat [nx x T] - stimulus for each time bin
%   ydat [ny x T] - response for each time bin (one-hot representation)
%
% Outputs:
% -------
%    logp [1 x 1] - log marginal probability: log p(X|theta)
%    gams [k x T] - marginal posterior over state at each time: p(z_t|X)
%  xissum [k x k] - summed marginal over pairs of states: p(z_t, z_{t+1}|X)
%      Cs [1 x T] - conditional marginal li: p(y_t | y_{1:t-1}) 

% Extract sizes
nStates = size(A,1);  
nT = size(xdat,2);
nY = size(ydat,1);  % assumed to be 2 for now!!

% Set initial state probability
pi0 = ones(1,nStates)/nStates; % assume uniform prior over initial state

% Compute GLM probabilities (nT x nStates x 2)
Py = zeros(nT,nStates,nY);
Py(:,:,2) = xdat'*W;
Py = exp(bsxfun(@minus,Py,max(Py,3))); % exponentiate
Py = bsxfun(@rdivide,Py,sum(Py,3)); % normalize to make probabilities
Py = sum(Py.*permute(ydat,[2 3 1]),3); % limit to observed likelihood terms only


%% Forward pass

aa = zeros(nStates,nT); % forward probabilities p(z_t | y_{1:t})
bb =  zeros(nStates,nT); % backward: p(y_{t+1:T} | z_t)/p(y_{t+1:T} | y_{1:T})
cs = zeros(1,nT); % forward marginal likelihoods: p(y_t|y_{1:t-1})

% First bin
pxz = pi0.*Py(1,:,ydat(:,1)); % joint probability P(y_1,z_1) 
cs(1) = sum(pxz); % normalizer
aa(:,1) = pxz'/cs(1); % conditional P(z_1|y_1) 

% Remaining time bins
for jj = 2:nT
    aaprior = aa(:,jj-1)'*A;  % propagate uncertainty forward
    pxz = aaprior.*Py(jj,:); % joint P(y_{1:t},z_t)
    cs(jj) = sum(pxz);  % conditional P(y_t|y_{1:t-1})
    aa(:,jj) = pxz'/cs(jj); % conditional P(z_t|y_{1:t})
end

%% Backward pass 
bb(:,nT) = 1; % set final column to all 1s
for jj = nT-1:-1:1
    bb(:,jj) = (A*(bb(:,jj+1).*Py(jj+1,:)'))/cs(jj+1);
end

%% Compute outputs

% marginal likelihood
logp = sum(log(cs));

% marginal posterior over states p(z_t | Data)
gams = aa.*bb; 

% Compute only sum of xis 
if nargout > 2
     xisum = zeros(nStates,nStates); % P(z_n-1,z_n|y_1:T)
    for jj = 1:nT-1
        xisum = xisum + (aa(:,jj) * (bb(:,jj+1)'.*Py(jj+1,:)) .*A)/cs(jj+1);
    end
end

% % OPTIONAL: compute posterior over all adjacent states pairs, 
% % p(z_t, z_t+1 | Data) for all t \in [1 nT-1].
% if nargout > 2
%     xis = zeros(nStates,nStates,nT-1); % P(z_n-1,z_n|y_1:T)
%     for jj = 1:nT-1
%         xis(:,:,jj) = (aa(:,jj)*(bb(:,jj+1)'.*Phi(ydat(:,jj+1),:)).*A)/cs(jj+1);
%     end
% end



