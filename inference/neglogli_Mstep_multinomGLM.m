function [negL,dnegL,H] = neglogli_Mstep_multinomGLM(wts,X,Y,pstate)
% [negL,dL,H] = neglogli_Mstep_multinomGLM(wts,X,Y,pstate)
%
% Weighted negative log-likelihood for multinomial logistic regression
% model, plus gradient and Hessian, for use in M-step.
%
% Inputs:
% -------
%    wts [d*(k-1),1] - weights mapping stimulus to each of m classes
%      X [T,d]       - design matrix of regressors
%      Y [T,k]       - output (one-hot vector on each row indicating class)
% pstate [T,1]       - latent state probability (weighting for LL terms)
%
% Outputs:
% --------
%    negL [1,1] - negative loglikelihood
%   dnegL [d,1] - gradient
%       H [d,d] - Hessian
%
% Details: 
% --------
% Describes mapping from vectors x to a discrete variable y taking values
% from one of k classes:
%
%     P(Y = j|x) = 1/Z exp(x*w_j)
%
% where normalzer Z = sum_i=1^k  exp(x*w_i)
%      
% Notes:
% ------
%
% - assumes weights for 1st class are 0.  So wts(:,j) are for class j+1
%
% - Constant ('offset' or 'bias') not added explicitly, so regressors X
%   should include a column of 1's to incorporate a constant.


% Process inputs
nT = size(X,1);      % # of trials
nX = size(X,2);      % # of predictors (input dimensionality)
nObs = size(Y,2);    % # of observation classes minus 1
nwtot = nX*(nObs-1); % total number of weights in the model

% Reshape GLM weights into a matrix
ww = reshape(wts,nX,nObs-1); 

% Compute projection of stimuli onto weights
xproj = X*ww;

if nargout <= 1   % === compute neg log-likeli only ======================
    
    negL = pstate'*(-sum(Y(:,2:end).*xproj,2) + multisoftplus(xproj));
    
elseif nargout >= 2 % === compute gradient ===============================
    
    [f,df] = multisoftplus(xproj); % evaluate log-normalizer & deriv

    negL = pstate'*(-sum(Y(:,2:end).*xproj,2) + f); % negative log-li
    
    dnegL = X'*((df-Y(:,2:end)).*pstate);  % gradient
    dnegL = dnegL(:);                      % reshape into colum vector

end

if nargout == 3 % ===== compute Hessian ==================================
    
%     [f,df,ddf] = softplus(xproj); % evaluate log-normalizer
%     negL = -(pstate.*Y)'*xproj + sum(pstate.*f); % neg log-likelihood
%     dnegL = X'*((df-Y).*pstate);                 % gradient
%     H = X'*bsxfun(@times,X,pstate.*ddf);    % Hessian
    
    % Compute stimulus weighted by df for each clas
    dftensor = permute(df,[1,3,2]); % put df into 3-tensor
    Xdf = reshape(X.*dftensor,nT,nwtot); % X times df tensor
    pXdf = pstate.*Xdf; % X times df tensor, weighted by state probabilities
    
    % Multiply by stimulus (center blocks)
    XXdf = X'*pXdf; % for blocks along center
    
    % Insert center block-diagonal portion of Hessian
    H = zeros(nwtot);
    for jj = 1:(nObs-1)
        inds = (jj-1)*nX+1:jj*nX;
        H(inds,inds) = XXdf(:,inds);
    end
    
    % Add off-diagonal blocks of Hessian
    H = H-Xdf'*pXdf;


end

