function [Ahat,What,logp,logpTrace,jj,dlogp,gams] = runEMforGLMHMM(A0,W0,xdat,ydat,optsEM)
% [Ahat,What,logp,logpTrace,jj,dlogp,gams] = runEMforGLMHMM(A0,W0,xdat,ydat,optsEM)
%4
% run EM for GLM-HMM with Bernoulli observations
%
% Inputs:
% -------
%    A0 [k x k] - initial state transition matrix
%    W0 [d x k] - initial weight matrix
%  xdat [d x T] - input stimuli (each column is a stimulus)
%  ydat [m x T] - data (each column is one-hot vector)
%  opts [struct] - optimization params (optional)
%       .maxiter - maximum # of iterations 
%       .dlogptol - stopping tol for change in log-likelihood 
%       .display - how often to report log-li
%
% Outputs:
% --------
%      Ahat [k x k] - estimated state transition matrix
%      What [d x k] - estimated GLM regression weights
%      logp [1 x 1] - log-likelihood
% logpTrace [1 x maxiter] - trace of log-likelihood during EM
%        jj [1 x 1] - final iteration
%     dlogp [1 x 1] - final change in log-likelihood
%      gams [k x T] - marginal state probabilities from last E step


% Set EM optimization params if necessary
if nargin < 5
    optsEM.maxiter = 200;
    optsEM.dlogptol = 0.01;
    optsEM.display = inf;
end
if ~isfield(optsEM,'display') || isempty(optsEM.display)
    optsEM.display = inf;
end

% Initialize params
nStates = size(A0,1);
Ahat = A0;
What = W0;

% Set up variables for EM
logpTrace = zeros(optsEM.maxiter,1); % trace of log-likelihood
dlogp = inf; % change in log-likelihood
logpPrev = -inf; % prev value of log-likelihood
jj = 1; % counter

% Set up loss function for GLM log-likelihood
optsFminunc = optimoptions(@fminunc,'Algorithm','trust-region','SpecifyObjectiveGradient',true,'HessianFcn','objective','display','off');
yclass = ydat(2,:)'; % extract 2nd row, so y input is binary (0 vs 1)

while (jj <= optsEM.maxiter) && (dlogp>optsEM.dlogptol)
    
    % --- run E step  -------
    %[logp,gams,xisum] = runFB_GLMHMM(Ahat,What,xdat,ydat); % standard version
    [logp,gams,xisum] = runFB_GLMHMM_stable(Ahat,What,xdat,ydat); % stable version
    logpTrace(jj) = logp;
   
    % --- run M step  -------
   
    % Update transition matrix
    Ahat = xisum ./ sum(xisum,2); % normalize each row to sum to 1
    
    % Update GLM weights (could be parallelized over states)
    for iistate = 1:nStates
        lfunc = @(w)neglogli_Mstep_bernoulliGLM(w,xdat',yclass,gams(iistate,:)'); % neglogli function handle
        What(:,iistate) = fminunc(lfunc,What(:,iistate),optsFminunc);
    end

    % ---  Display progress ----
    if mod(jj,optsEM.display)==0
        fprintf('EM iter %d:  logli = %-.6g\n',jj,logp);
    end
    
    % Update counter and log-likelihood change
    jj = jj+1;  
    dlogp = logp-logpPrev; % change in log-likelihood
    logpPrev = logp; % previous log-likelihood

    if dlogp<-1e-6
        warning('Log-likelihood decreased during EM!');
        fprintf('dlogp = %.5g\n', dlogp);
    end

end
jj = jj-1;

