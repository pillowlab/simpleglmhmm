function [negL,dnegL,H] = neglogli_Mstep_bernoulliGLM(wts,X,Y,pstate)
% [negL,dnegL,H] = neglogli_Mstep_bernoulliGLM(wts,X,Y,pstate)
%
% Compute negative log-likelihood of data under logistic regression model,
% plus gradient and Hessian
%
% Inputs:
% -------
%    wts [d x 1] - regression weights
%      X [N x d] - regressors
%      Y [N x 1] - output (binary vector of 1s and 0s).
% pstate [N x 1] - probability of being in state at each time bin (serve as weights)
%
% Outputs:
% -------
%   negL [1 x 1] - negative log-likelihood
%  dnegL [d x 1] - gradient of negative log-likelihood
%      H [d x d] - Hessian (2nd derivative matrix)

% Compute projection of stimuli onto weights
xproj = X*wts;

if nargout <= 1    % Evaluate neglogli only
    
    negL = -(pstate.*Y)'*xproj + sum(pstate.*softplus(xproj)); % neg log-likelihood

elseif nargout == 2 % Evaluate gradient
    
    [f,df] = softplus(xproj); % evaluate log-normalizer & deriv
    negL = -(pstate.*Y)'*xproj + sum(pstate.*f); % neg log-likelihood
    dnegL = X'*((df-Y).*pstate);         % gradient

elseif nargout == 3 % Evaluate gradient & Hessian
    
    [f,df,ddf] = softplus(xproj); % evaluate log-normalizer & derivs
    negL = -(pstate.*Y)'*xproj + sum(pstate.*f); % neg log-likelihood
    dnegL = X'*((df-Y).*pstate);                 % gradient
    H = X'*bsxfun(@times,X,pstate.*ddf);         % Hessian
    
end

