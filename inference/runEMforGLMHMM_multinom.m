function [Ahat,What,logp,logpTrace,jj,dlogp,gams] = runEMforGLMHMM_multinom(A0,W0,xdat,ydat,optsEM)
% [Ahat,What,logp,logpTrace,jj,dlogp,gams] = runEMforGLMHMM_multinom(A0,W0,xdat,ydat,optsEM)
%
% run EM for GLM-HMM with multinomial-GLM observations
%
% Inputs:
% -------
%    A0 [k,k]     - initial state transition matrix
%    W0 [d,q-1,k] - initial weights (d inputs, q output classes, k states)
%  xdat [d,T]     - input stimuli (each column is a stimulus)
%  ydat [m,T]     - data (each column is one-hot vector)
%  opts [struct] - optimization params (optional)
%       .maxiter - maximum # of iterations 
%       .dlogptol - stopping tol for change in log-likelihood 
%       .display - how often to report log-li
%
% Outputs:
% --------
%      Ahat [k,k] - estimated state transition matrix
%      What [d,q-1,k] - estimated GLM weights
%      logp [1,1] - log-likelihood
% logpTrace [1,maxiter] - trace of log-likelihood during EM
%        jj [1,1] - final iteration
%     dlogp [1,1] - final change in log-likelihood
%      gams [k,T] - marginal state probabilities from last E step


% Set EM optimization params if necessary
if nargin < 5
    optsEM.maxiter = 200;
    optsEM.dlogptol = 0.01;
    optsEM.display = inf;
end
if ~isfield(optsEM,'display') || isempty(optsEM.display)
    optsEM.display = inf;
end

% Extract sizes
nStates = size(A0,1);
nObs = size(ydat,1);

% Check shape of W for Bernoulli vs. Multinomial 
if (nObs==2) && (size(W0,3)==1) && (size(W0,2)==nStates)
    % Bernoulli case: permute indices so each page has per-state weights
    W0 = permute(W0,[1 3 2]);  
elseif (size(W0,3)~=nStates) || (size(W0,2)~=(nObs-1))
    error('Weights matrix size mismatch to number of states & observations');
end

% Initialize params
Ahat = A0;
What = W0;

% Set up variables for EM
logpTrace = zeros(optsEM.maxiter,1); % trace of log-likelihood
dlogp = inf; % change in log-likelihood
logpPrev = -inf; % prev value of log-likelihood
jj = 1; % counter

% Set optimization parameters for M-Step
optsFminunc = optimoptions(@fminunc,'Algorithm','trust-region','SpecifyObjectiveGradient',true,'HessianFcn','objective','display','off');

% Specify negative log-likelihood function for M-step
if (nObs == 2)
    % Bernoulli
    negloglifun = @(w,gamvec)neglogli_Mstep_bernoulliGLM(w,xdat',ydat(2,:)',gamvec); 
else
     % Multinomial 
    negloglifun = @(w,gamvec)neglogli_Mstep_multinomGLM(w,xdat',ydat',gamvec);
end

% ================================================================
% BEGIN EM ITERATIONS
% ================================================================
while (jj <= optsEM.maxiter) && (dlogp>optsEM.dlogptol)
    
    % ------- E step  ----------------------------
    [logp,gams,xisum] = runFB_GLMHMM_stable(Ahat,What,xdat,ydat); % stable version
    logpTrace(jj) = logp;

    % ------- M step  ----------------------------
   
    % Update transition matrix
    Ahat = xisum ./ sum(xisum,2); % normalize each row to sum to 1
   
    % Optimize GLM weights (could be parallelized over states)
    for iistate = 1:nStates
            Wstate0 = What(:,:,iistate); % initial weights for this state

            lfunc = @(w)negloglifun(w,gams(iistate,:)'); % neglogli function handle
            WstateOpt = fminunc(lfunc,Wstate0(:),optsFminunc); % run optimization
            
            What(:,:,iistate) = reshape(WstateOpt,[],nObs-1); % re-insert
    end
    
    % -----  Display progress ----------------------
    if mod(jj,optsEM.display)==0
        fprintf('EM iter %d:  logli = %-.6g\n',jj,logp);
    end
    
    % Update counter and log-likelihood change
    jj = jj+1;  
    dlogp = logp-logpPrev; % change in log-likelihood
    logpPrev = logp; % previous log-likelihood

    if dlogp<-1e-6
        warning('Log-likelihood decreased during EM!');
        fprintf('dlogp = %.5g\n', dlogp);
    end

end
jj = jj-1; % decrement # of iters by 1

