% Test EM algorithm for fitting a GLM-HMM to simulated data
%  JP (Jun 2019).

addpath tools; % add directories
addpath inference;

%% 1. Generate simulated dataset

% Set parameters: transition matrix and emission matrix
nStates = 2; % number of states
nObs = 2; % number of observation classes (2 = Bernoulli)
nX = 10;  % number of input dimensions
nT = 2e4; % number of time bins

% Generate data from a simulated HMM
[mm,xstim,zlatent,yclass,y1hot] = genSimGLMHMMdata(nStates,nObs,nX,nT);
stateFracs = [sum(zlatent==1), sum(zlatent==2)]/nT;
fprintf('--------------\nState 1: %.1f%%\nState 2: %.1f%%\n',stateFracs*100);

% Plot params and data

subplot(221);
imagesc(mm.A); axis image; 
xlabel('state at time t+1'); ylabel('state at time t');
set(gca,'xtick',1:nStates,'ytick',1:nStates);
title('transition matrix A'); colorbar;

subplot(223);
tw = 1:nX; tz = 1:nStates;
plot(tw, mm.Wts, 'linewidth', 2);
hold on; plot(tw, tw*0, 'k--'); hold off;
xlabel('regressor'); ylabel('weight');
%set(gca,'xtick',tw);
title('GLM weights');
labels = {'state 1', 'state 2','state 3','state 4'};
legend(labels{1:nStates});

subplot(222);
lw = 2; tt = 1:100; % number of time bins to plot
plot(tt, zlatent(tt), '-o', 'linewidth', lw)
set(gca,'ytick',tz,'ylim',[0 nStates]+0.5);
title('true latent'); ylabel('state');

subplot(224);
plot(tt, yclass(tt), '-o', 'linewidth', lw);
set(gca,'ytick',tw,'ylim',[0 1]+0.1);
title('observations'); xlabel('time'); ylabel('observation');
drawnow;

%% 2. Fit standard multinomial GLM (ie 1-state model)

fprintf('\n------------------------\n');
fprintf('Fitting Bernoulli GLM...\n');
fprintf('------------------------\n');

% Set optimization options
opts1 = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','display','iter');

% Do optimization
lfunc = @(w)(neglogli_bernoulliGLM(w,xstim',yclass')); % neglogli function handle
wts0 = randn(nX,1)*.1; % initial weights
[Wglm_ml,nlogli,H] = fminunc(lfunc,wts0,opts1); % run optimization

% Make plot
plot(tw,mm.Wts,'linewidth',2);
hold on; plot(tw,Wglm_ml,'k--','linewidth',2); hold off;
xlabel('coeffient'); title('weights');
legend('W_1', 'W_2', 'W_{glm}'); drawnow;


%% 3. Test forward backward and M step starting from true params

% Run forward-backward w/ true params to get optimal latent probabilities 
[logpTrue,gamsTrue] = runFB_GLMHMM_stable(mm.A,mm.Wts,xstim,y1hot);

% ----------------------------------------------
% Plot and track results during EM (if desired):
% ----------------------------------------------

% subplot(223);
% imagesc(tt,1:nStates,gamsTrue(:,tt)); hold on; axis xy;
% plot(tt, zlatent(tt), '-o', 'linewidth', lw); 
% hold off;
% title('posterior over states given true params');
% set(gca,'ytick',1:nStates);
% ylabel('state'); xlabel('time');
% 
% % Run M-step to fit GLM weights (using gamsTrue) from true params
% WtrueML = zeros(nX,nStates);
% for iistate = 1:nStates
%     fprintf('--- Maximizing weighted log-likelihood (state %d) --- \n',iistate);
%     lfunc2 = @(w)neglogli_Mstep_bernoulliGLM(w,xstim',yclass',gamsTrue(iistate,:)'); % neglogli function handle
%     WtrueML(:,iistate) = fminunc(lfunc2,Wglm,opts1);
% end
% 
% subplot(224);
% plot(tw,mm.Wts,'linewidth',2);
% h = get(gca,'colororder'); set(gca,'colororder',h(1:2,:));
% hold on;  plot(tw,WtrueML,'--', 'linewidth',2);
% plot(tw,Wglm,'k--','linewidth',2); hold off;
% xlabel('coeffient'); title('weights');
% legend('W_1', 'W_2', 'W_{ml1}', 'W_{ml2}','W_{glm}');


%% 4. Run EM to estimate model params from a random initialization

fprintf('\n------------------------------------------------\n');
fprintf('Running EM initialized from GLM fit (+ noise)...\n');
fprintf('------------------------------------------------\n');

optsEM.maxiter = 100;  % max # of EM iterations
optsEM.dlogptol = 1e-3; % stop when change in log-likelihood falls below this
optsEM.display = 10;

% Initialize A
A0 = 1*eye(nStates)+.2*rand(nStates);
A0 = A0 ./ sum(A0,2); % normalize so rows sum to 1

% Initialize Weights
W0 = Wglm_ml + randn(nX,nStates)*norm(Wglm_ml)*.25;

% --- run EM -------
tic;
[Ahat,What,logp,logpTrace,jStop,dlogp,gams] = runEMforGLMHMM(A0,W0,xstim,y1hot,optsEM);
toc;

%% 5. Display results

% Permute states to find best match to true model states
Mperm = computeAlignmentPermutation(mm.Wts,What);
Ahat = Mperm*Ahat*Mperm';
What = What*Mperm';

% ---- make plots -------
subplot(223);
plot([1 min(jStop+20,optsEM.maxiter)],logpTrue*[1 1], 'k',...
    1:jStop,logpTrace(1:jStop),'-', 'linewidth', lw);
xlabel('EM iteration');
ylabel('log p(Y|theta)');
title('EM path');

if jStop==optsEM.maxiter
    fprintf('EM terminated after max # iters (%d) reached (dlogp = %.4f)\n',jStop,dlogp);
else
    fprintf('EM terminated after %d iters (dlogp = %.4f)\n',jStop,dlogp);
end
fprintf('\nrelative final log-likelihood: %.2f \n', logp-logpTrue);
if logp>logpTrue, fprintf('(SUCCESS!)\n');
else,   fprintf('(FAILED to find optimum!)\n');
end
    
subplot(224);
plot(tw,mm.Wts,'linewidth',2);
h = get(gca,'colororder'); set(gca,'colororder',h(1:2,:));
hold on;  plot(tw,What,'--', 'linewidth',2); hold off;
xlabel('coeffient'); title('weights');
legend('W_1', 'W_2', 'What_1', 'What_2');
