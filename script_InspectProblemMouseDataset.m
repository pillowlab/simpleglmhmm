% Test EM algorithm for fitting a GLM-HMM to simulated data using multiple
% initializations

%  JP (Jun 2019).

addpath tools; % add directories

%% ====  1. Load data =====================
fprintf('\n --------\n Loading Data\n');
%rawdat = csvread('data/CSHL_005.csv',2,1);
rawdat = csvread('data/badMouseData.csv');
rawdat(1,2) = 0; 

% extract choice data
yclass = double(rawdat(:,7)');

% extract regressors to use
iix = [1:3, 6];
nX = length(iix); % number of regressors to use
xstim = rawdat(:,iix)'; % stimulus
nT = size(xstim,2);
rlabels = {'X','prevX','prevY','prevR','is20','bias'};

y1hot = full(convertToOneHot(yclass+1));
choiceFractions = full(mean(y1hot,2))';
fprintf('\nChoice percentages (L,R): [ ');
fprintf('%.1f ', 100*choiceFractions);
fprintf('] %%\n');
pctCorrect = (1-mean((rawdat(:,1)<0)==yclass'))*100;
fprintf('Percent correct: %.1f%%\n\n',pctCorrect);

% --- plot marginal response dist ----
clf; subplot(221);
bar(1:2, 100*choiceFractions);
set(gca,'xtick',1:2,'xticklabel',{'L','R'});
ylabel('% correct'); title('responses');

%% ===== 2. Fit Bernoulli GLM ========================
fprintf('------------\nFitting GLM...\n');

lfunc = @(w)(neglogli_bernoulliGLM(w,xstim',yclass')); % neglogli function handle
opts1 = optimoptions(@fminunc,'Algorithm','trust-region',...
    'SpecifyObjectiveGradient',true,'HessianFcn','objective','display','iter');
wts0 = randn(nX,1)*.1;
[Wglm,nlogli,H] = fminunc(lfunc,wts0,opts1);

% Plot weights
subplot(222); 
tw = 1:nX; lw = 2;
plot(tw,tw*0, 'k--', tw,Wglm,'k','linewidth',lw);
title('GLM weights'); ylabel('weight');
set(gca,'xtick',1:nX,'xticklabel',rlabels(iix)); drawnow;


%% ===== 3. Fit GLM-HMM using EM ========================

nStates = 2;  % number of states to use
nrpts = 10; % number of times to run EM

% Initialize EM from true params to see upper bound (presumably) on logli
optsEM.maxiter = 100;  % max # of EM iterations
optsEM.dlogptol = 1e-6; % stop when change in log-likelihood falls below this
optsEM.display = 10; % how often to report logli

pTrace = zeros(optsEM.maxiter,nrpts); % stored log-likelihood traces
logp_store = zeros(nrpts,1);
What_store = zeros(nX,nStates,nrpts);
Ahat_store = zeros(nStates,nStates,nrpts);
LLDecrease = zeros(nrpts,1);

fprintf('\n------------------\nRunning EM with random initializations:\n');
fprintf('Repeat #: \n');
subplot(223); cla; hold on;
for jj = 1:nrpts

    % Initialize A
    A0 = eye(nStates)+.1*rand(nStates);
    A0 = A0 ./ sum(A0,2); % normalize so rows sum to 1

    % Initialize Weights
    W0 = Wglm + randn(nX,nStates)*norm(Wglm)*.25;
    
    % --- run EM -------
    [Ahat,What,logp,logpTrace,jStop] = runEMforGLMHMM(A0,W0,xstim,y1hot,optsEM);

    % --- store outputs -----
    pTrace(:,jj) = logpTrace'; % store log likelihood trace
    What_store(:,:,jj) = What;
    Ahat_store(:,:,jj) = Ahat;
    logp_store(jj) = logp;
    if any(diff(logpTrace(1:jStop))<0)
        warning('Log-likelihood decreased during EM!');
        fprintf('Decrease = %g\n', min(diff(logpTrace(1:jStop))));
    end
    
    % ---- make plots -------
    plot(1:jStop,logpTrace(1:jStop),'-', 'linewidth', lw); 
    xlabel('EM iteration');
    ylabel('log p(Y|theta)');
    drawnow;
    
    fprintf(' %3d ', jj);
    if mod(jj,10)==0
        fprintf('\n');
    end

end
fprintf('\n\n');
hold off;

% Select best fit based on highest marginal likelihood
[logpML,imax] = max(logp_store);
WhatML = What_store(:,:,imax);
AhatML = Ahat_store(:,:,imax);

%% ======= 5. Make plots ======================

subplot(222); 
tw = 1:nX; lw = 2;
h = plot(tw,tw*0, 'k--', tw,Wglm,'k',tw,What,'linewidth',lw);
title('weights');
ylabel('weight');
set(gca,'xtick',1:nX,'xticklabel',rlabels(iix));
legend(h(2:4), 'W_{glm}', 'W_1','W_2');

subplot(224);
plot(1:nStates^2, AhatML(:), 'linewidth', lw);
xlabel('matrix entry #');
ylabel('value');
title('Transition matrix');
set(gca,'xtick', 1:nStates^2);

% Compute null model log-likelihood (given just rate of L and R choices)
logliNull = full(sum(sum(y1hot'.*log(choiceFractions)))); % null log-likelihood 

subplot(221);
hist((logp_store-logliNull)/nT);
title('log-likelihoods (bits/trial)')
xlabel('(logp - logpnull)/nT');
ylabel('count');
